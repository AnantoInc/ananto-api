package com.minsa.minsaapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MinsaApiApplication

fun main(args: Array<String>) {
    runApplication<MinsaApiApplication>(*args)
}
